import { Schema,SchemaTypes,model } from "mongoose";
const MessageSchema=new Schema({
message:{
    type:String,
     required:true
},
receiverId:SchemaTypes.ObjectId
},{timeseries:true})
export const MessageModel=model("Message",MessageSchema)
