import { Schema,model } from "mongoose";
const UserSchema=new Schema({
name:{
    type:String,
    minLength:[3,'this Not Enghoe'],
    maxLength:[30,'Longer'],
    required:true
},
email:{
    type:String,
    required:true
},
password:{
    type:String,
        required:true
},
isactive:{
    type:Boolean,
    required:true,
    default:false
},
age:{
    type:Number,
    required:true,
    min:3,
    max:80
}
},{timeseries:true})

export const Users=model("user",UserSchema)
