import express from "express";
import { configDotenv } from "dotenv"; 
import { DBconnection } from "./Database/DBconnection/DBconnection.js";
import  userRouter  from "./src/module/user/user.router.js";
import { UseRouterMessage } from "./src/module/message/message.router.js";
import { AppError } from "./src/util/AppError.js";
import { Users } from "./Database/modalSchema/usermodal.js";
import { MaddelwareError } from "./src/maddelware/MAdellwareError.js";
configDotenv()

const app=express();
app.use(express.json())
app.get("/",(req,res)=>res.send('hellowored'))
app.get("/verfiy/:email",async(req,res)=>{
    await Users.findOneAndUpdate({email:req.params.email},{isactive:true})
})
DBconnection()
app.use(userRouter)
app.use('/Message',UseRouterMessage)
app.use('/*',(req,res,next)=>{
    next(new AppError('Not Found',404))
})
app.use(MaddelwareError)

const port=3000
app.listen(port,()=>console.log('done'))
process.on('ubhandledRejection',(error)=>{
    console.log('error0',error)
})
