import Joi from 'joi' 
export const validateSignUp=Joi.object({
name:Joi.string()
.alphanum()
.min(3)
.max(30)
.required(),
email:Joi.string()
.email({ tlds: { allow: ['com', 'net'] } }) ,
password: Joi.string()
.pattern(new RegExp('^[a-zA-Z0-9]{3,30}@[A-Z]{2,}$')),
age:Joi.number().min(3)
.max(30)
.required(),
})
export const validateSignIn=Joi.object({
    email:Joi.string()
    .email({ tlds: { allow: ['com', 'net'] } }),
    password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}@[A-Z]{2,}$')),
   })