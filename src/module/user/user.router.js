import express from "express";
import { Validate } from "../../maddelware/Validate.js";
import { SignIn, signUp } from "./user.contorl.js";
import { validateSignIn, validateSignUp } from "./user.vaildate.js";
 const userRouter=express.Router()
userRouter.post('/signUp',Validate(validateSignUp),signUp)
userRouter.post('/signIn',Validate(validateSignIn),SignIn)

export default userRouter