import { MessageModel } from "../../../Database/modalSchema/messagemodel.js";
import { catchError } from "../../util/catchError.js";

const AddMessage=catchError(async (req,res)=>{
    const {message,receiverId}=req.body
    MessageModel.insertMany({message,receiverId})
    res.json({message:"Success"})
})

const GetMessage=catchError(async(req,res)=>{
    const datauser=await MessageModel.find({receiverId:req.userId})
   res.json({data:datauser})
})
export {
    AddMessage,
    GetMessage
}