import express from "express";
import { AddMessage, GetMessage } from "./message.control.js"
import { Validate } from "../../maddelware/Validate.js"
import { validateMessage } from "./message.validate.js"
import { Auth } from "../../maddelware/Auth.js";

export const UseRouterMessage=express.Router()
UseRouterMessage.post('/',Validate(validateMessage),AddMessage)
UseRouterMessage.get('/',Auth,GetMessage)
 