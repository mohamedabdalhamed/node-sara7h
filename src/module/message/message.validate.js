import Joi from 'joi' 
export const validateMessage=Joi.object({
    message:Joi.string()
.min(3)
.max(500)
.required(),
receiverId:Joi.string()
.required(),
})
