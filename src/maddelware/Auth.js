import Jwt from "jsonwebtoken"
export const Auth=(req,res,next)=>{
    const  token=req.header('token')
    Jwt.verify(token,process.env.Jwt_Key,async(error,decode)=>{
        if(error) return res.json({error:error})
        if(decode.isactive==false)  res.json({message:"active ur acount"})
        req.userId=decode.userId
        next()
    })



}