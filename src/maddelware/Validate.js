export const Validate=(Schema)=>{
return (req,res,next)=>   {
    let {error}=Schema.validate(req.body,{abortEarly:false})
if(!error) return next()
 res.json({message:error})
} 
}